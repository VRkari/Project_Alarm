#include <iostream>
#include <string>

void compare(std::string str1, std::string str3);

int main(){

	std::string str1 = "Hello world";
	std::string str2 = "Testing compare";
	std::string str3 = "Hello world";

	if(str1.compare(str2) == 0){
		std::cout<<"Equal"<<std::endl;
	}
	else{
		std::cout<<"Not equal"<<std::endl;
	}

	compare(str1, str3);

	
}

void compare(std::string str1, std::string str3){
	if(str1.compare(str3) == 0){
		std::cout<<"Functional equality"<<std::endl;
	}
}


