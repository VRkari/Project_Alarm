#include <iostream>
#include <fstream>
#include <vector>
#include "headers.h"

enum UserStatus{active = 1, inActive = 2, blocked = 3};

struct UserData{			//enum and struct can be in seperate .h file :)
	int ID;
	int pinc;
	std::string nName;
	int tagid;
	UserStatus status;
	int reserved;
};

void import(std::vector<UserData>& imp);
bool pinchecking(struct Userdata data, int pincode);

int main(){

	std::vector<UserData> imp;

	import(imp);

	return 0;
}

void import(std::vector<UserData>& imp){

	std::ifstream infile;
	UserData data; //structure variable created
	std::string line; //string to hold the variables read from infile
	//open file in directory Bin
	infile.open("../Bin/Users.dat");
		if(infile.is_open() ){
			//reads from file and saves in vector import
			while(std::getline(infile, line)){
				imp.push_back(data);
			}
		//close file
		infile.close();
		}
}

bool pinchecking(struct Userdata data, int pincode){    //check the pincode with the code read from the .dat file

	std::cin>>pincode;

	for(int i = 1; i < 3; i++){

		if(pincode != data.pinc && i < 3){
			std::cout<<"\nTry "<<i+1<<" , try again: ";
		}

		else if(pincode == data.pinc && i < 3){
			return true;
		}
		else (i == 3){
			return false;
		}
		std::cin>>pincode;
	}


}
