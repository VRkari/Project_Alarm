#include <iostream>
#include <vector>
#include <fstream>
#include <ctime>
#include <string>
#include "headers.h"


bool logg(std::vector<UserData> &data){
	
	UserData databank;
	int loggnr = 0;
	std::string userID;
        int reserved;
        std::string loggtxt;
	bool attemptlogin = true;

	//declaring time + date
	time_t now = time(0); //gets time from the OS
	//converting now to string form
	char* date = ctime(&now);

	//define userID
	userID = databank.ID;

	std::ofstream logg;

	logg.open("../Bin/system.log"); //create a file with log output

	//a condition to register every attempt + succesfull attempt of login in
	while(attemptlogin){
		loggnr++; //increases attempt +1 starting from 0
		
		if(attemptlogin == false){
	
			loggtxt = "Failed attempt to log-in";

			return true; //continue while loop
		}

		else{
			loggtxt = "Succesfull login";
			return false; //break the loop
		}

		//simple log line
		logg << loggnr << ";"  << date << ";" << userID << ";" << loggtxt <<std::endl;
	}


	logg.close(); //closes the file
}
