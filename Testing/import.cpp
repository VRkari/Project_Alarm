#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "headers.h"


struct UserData import(std::vector<UserData> &databank){

	struct UserData tmp;  //temporare saves files
	std::ifstream infile;

		//open file in directory Bin
		infile.open("../Bin/Users.dat");
			if(infile.is_open()){
				//reads from file and saves in struct UserData
				int i = 0;
				while(!infile.eof() ){

					if(infile.fail() ){ //if the file fails to open, itll output an error message
						std::cout<<"Error opening file" << std::endl;
						exit(1);
					}

					std::getline(infile, tmp.ID, ';');  //items that get saves into the struct
					std::getline(infile, tmp.pinc, ';');
					std::getline(infile, tmp.nName, ';');
					std::getline(infile, tmp.tagid, ';');
					std::getline(infile, tmp.status, ';');
					std::getline(infile, tmp.reserved, '\n');
					i++;

					std::cout<<i<<": reading user... processing..."<<std::endl;

					databank.emplace_back(tmp); //puts the values read at the back of the vector
				}
			//close file
			infile.close();
			}
}
/*
int main(){

	std::vector<UserData> databank;
	
	import(databank);
}*/
