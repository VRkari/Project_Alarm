Welcome to the README of my alarm system.

In the original alarm which contains the files:
 -Alarm_01.cpp
 -clearance.cpp
 -menu.cpp
 -headers.h

The user will be promptly asked to enter his or her name. When this is entered the password will be requested.
The user will have 3 tries to enter the right password before the program will close with an error message.
If succesfull the alarm will be turned off and the menu for further actions will pop up.
The actuall meny doesn't have working functions yet besides the turning on of the alarm.

In the newest files:
 -userimp.cpp
 -userlog.cpp

The user will be able to create a .dat file with information following the same setup:
=> User ID; pincode(Numbers only); Username(Letters only); Tag ID; Status; Reserved(empty).
which will get functionality later on the lifespan of this program.

The second file refers to the log of the program.
Here the program will output some of the information of the User who tries to logg in into the system.
the info profided in a seperate .log file will be provided in the following way:
=> LoggNr; Date, User ID; Reserved(Empty); Reserved(Empty); Logtext.

Currently all the 'Reserved' files have no functionality and will most likely be added in the future lifespan of the program.

At this point in time the Original alarm files function without the added userimp.cpp and userlog.cpp files. This will be added ASAP.

For questions, send a mail to: stijn.boons@outlook.com.

Thanks for reading and i hope you enjoy the system!
If you have any tips or recommendations, do send em forward to the afformentioned email adress.

Greetings,

Stijn 'VRkari' Boons
