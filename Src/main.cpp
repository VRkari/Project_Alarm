/**********************************
* Filename: Alarm.cpp
* 
* author: Stijn
* created: 2018-09-25
* notes:
*     
* desc:
* 
* ver: 2018-09-25 first version
* ver: 2018-10-16 second version
* ver: 2018-10-23 third version
*
* **********************************/
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "headers.h"


int main(){

    	//variables list
    	std::string name;

	//first attempt to (un-)lock.
    	alarmOn();

    	//variable that takes over the value returned by the function checkPin
    	bool succes = pinchecking();
    
    	//Output of the checkPin function
    	output(succes);
    
    	//options menu after right pin entered
    	while(succes){
    		//returns false when alarm is reactivated
    		succes = optionmenu();
    	}

	return 0;
}
