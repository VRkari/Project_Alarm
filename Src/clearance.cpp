#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "headers.h"


//intro screen to program and input first code attempt
void alarmOn(){

    std::cout << "******* !!THE ALARM IS ON!! ********\n";
    std::cout << std::endl;
    std::cout << "Enter your pincode[4-6 numbers]: ";
}

bool pinchecking(){    //check the entered pincode with the code from the userfile

	std::string pincode;
	std::cin>>pincode;

	std::string ID;
	std::string pinc;
	std::string nName;
	std::string tagid;
	std::string status;
	std::string reserved;

	//compares the string from struct with the string of cin
	for(int i = 1; i < 3; i++){
		//imports the data from Users.dat
		std::ifstream infile;

                //open file in directory Bin
                infile.open("../Bin/Users.dat");
                        if(infile.is_open()){
                                //reads from file and saves in struct UserData
                                int i = 0;
				std::string line;
                                while(!infile.eof() && std::getline(infile, line)){

                                        if(infile.fail() ){ //if the file fails to open, itll output an error message
                                                std::cout<<"Error opening file" << std::endl;
                                                exit(1);
                                        }
                                        std::getline(line, ID, ';');
                                        std::getline(line, pinc, ';');
                                        std::getline(line, nName, ';');
                                        std::getline(line, tagid, ';');
                                        std::getline(line, status, ';');
                                        std::getline(line, reserved, '\n');
					i++;
                                        std::cout<<i<<": reading user... processing..."<<std::endl;
                                }
                        //close file
                        infile.close();
                       	}
		//if not equal
		if((pincode.compare(pinc)!=0) && i < 3){ 
			std::cout<<"\nTry "<<i+1<<" , try again: ";
		}
		//if equal
		else if((pincode.compare(pinc)==0) && i < 3){
			return true;
		}
		//if too many attempts
		if (i == 3){
			return false;
		}
		std::cin>>pincode;
	}
		
		//Logging data
        	int loggnr = 0; 
        	std::string loggtxt;
        	bool attemptlogin = true;

        	//declaring time + date
        	time_t now = time(0); //gets time from the OS
        	//converting now to string form
	        char* date = ctime(&now);

        	std::ofstream logg;

        		logg.open("../Bin/system.log"); //create a file with log output

        		//a condition to register every attempt + succesfull attempt of login in
        		while(attemptlogin){
                		loggnr++; //increases attempt +1 starting from 0

                		if(attemptlogin == false){

                        		loggtxt = "Failed attempt to log-in";
	
        	                	return true; //continue while loop
                		}

                		else{
                        		loggtxt = "Succesfull login";
                        		return false; //break the loop
               			}

                	//simple log line
                	logg << loggnr << ";"  << date << ";" << ID << ";" << loggtxt <<std::endl;
       		 	}	

        		logg.close(); //closes the file

	return false;
}

//outputs depending on the value returned by checkPin
int output(bool succes){

	//if checkPin returns true:
    	if (succes == true){
        	std::cout << "\nWelcome home! " << "\nThe alarm has been turned off.\n";
    	}
    	//if checkPin function returns false, output:
    	else if ( succes == false){

        	std::cout << "\n***Alarm signal send to authorities***, \nPlease remove yourself from the property\n";
    	}

    	return 0;
}
