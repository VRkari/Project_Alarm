#include <iostream>
#include <vector>
#include "headers.h"


//if the alarm code was correct, this will be the next menu showing
int optionmenu(){

	int option;
	int choice;

	menu1:
    	std::cout<<"\n***** Menu *****\n";
    	std::cout<<"Make your choice: \n";
    	std::cout<<"1. Activate lock\n";
    	std::cout<<"2. Configuration\n";
    	std::cout<<"3. Exit\n";
    	std::cin>>option;
   
   	 switch(option){
        	case 1: 
            	std::cout<<"******!!ACTIVATED!!******\n";
            	return 0;
        	case 2:
            	menu2:
            	std::cout<<"\n***** Menu *****\n";
            	std::cout<<"Make your choice: \n";
            	std::cout<<"1. Add user\n";
            	std::cout<<"2. Change information of current user\n";
            	std::cout<<"3. Back\n";
            	std::cin>>choice;
            	
            	switch(choice){
                	case 1:
                    	std::cout<<"Enter the a new user: \n"; //add to User.dat
                    	return 0;
                	case 2:
                    	std::cout<<"What info would you like to change?\n"; //enter new menu to give an option which info to change(?)
                    	return 0;
                	case 3:
                    	goto menu1;
            	}
        	default: 
			break;
    	}
    	return 0;
}
