CC=g++
CCX=gcc
#only add 1 of the same variable in 1 name

CFLAGS = -c -Wconversion
CPPFLAGS = -o -Wall

INC_DIR = Inc
SRC_DIR = Src
BIN_DIR = Bin
OBJ_DIR = Obj

source = $(wildcard $(SRC_DIR)/*.cpp)
Bin_file = $(BIN_DIR)/alarm
Något = $(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(source))

all: $(Bin_file)

$(Bin_file) : $(Något)
	@echo "Compiling ..."
	@$(CC)  $^ -o $@ 	

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@echo "Compiling ..."
	@$(CC) -c -I$(INC_DIR) $< -o $@ 

clean:
	$(RM) $(Något)
